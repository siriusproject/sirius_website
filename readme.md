# Sirius Project Website

This website is just landing page for sirius project the plan is to build wordpress based enginee and create a template for it. Expantion plans would be : Voting plugins , login , register , forum , pools , blog

## Getting Started
Installing gulp dependencies:
Navigate to project root and run:

```
npm install
```
To run gulp compile:
```
gulp
```

### And coding style tests

Please write with LESS/SAAS/SCSS structure with markups eg.

```
div.someclass {
  span.someclass {
  
  }
}
```

## Authors

* **Tomasz Dolski** - *Initial work* - [Martians Company](https://martians.pl)
* **Son Pham** - *continue* - [Son Pham]
* **Other Sirius Community Member** - *continue* 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
