/**
 * Created by tony on 13/09/2017.
 */
(function sizes(){
    var height = 0;
    $('div.box').each(function(i,el){
        var input = $(el).height();
        if(input > height ) {
            height = input;
        }
    });
    $('div.box').height(height);
})();
$(window).on('resize' , function(){
    sizes()
});
$(window).on('load' , function(){
    var height = 0;
    $('div.box').each(function(i,el){
        var input = $(el).height();
        if(input > height ) {
            height = input;
        }
    });
    $('div.box').height(height);
});
(function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        loadParticles();
    }

    function loadParticles(){
        particlesJS.load('particles-js', 'particles.json', function() {
            console.log('callback - particles.js config loaded');
        });
    }
})();